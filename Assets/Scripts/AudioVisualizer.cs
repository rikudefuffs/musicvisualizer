﻿using UnityEngine;
using UnityEngine.UI;

namespace RikuTheFuffs
{
    public class AudioVisualizer : MonoBehaviour
    {
        public RectTransform[] bars;

        public float[] audioSpectrumData;
        public float multiplier;

        public float maxSpeed;
        public float maxHeight;
        float baseBarHeight;

        Image[] barsImages;

        public Color baseColor;
        public Color loudColor;

        [Tooltip("How fast will you go from baseColor to loudColor? This * barHeight should be = 1")]
        public float colorSensitivity;

        float deltaTime;

        void Start()
        {
            audioSpectrumData = new float[256];
            baseBarHeight = bars[0].sizeDelta.y;

            barsImages = new Image[bars.Length];
            for (int i = 0; i < barsImages.Length; i++)
            {
                barsImages[i] = bars[i].GetComponent<Image>();
            }
        }

        void Update()
        {
            AudioListener.GetSpectrumData(audioSpectrumData, 0, FFTWindow.Rectangular);
            deltaTime = Time.deltaTime;
            for (int i = 0; i < bars.Length; i++)
            {
                UpdateBarHeight(bars[i], audioSpectrumData[i]);
                UpdateBarColor(barsImages[i], bars[i].sizeDelta.y);
            }
        }

        void UpdateBarHeight(RectTransform bar, float audioSpectrumValue)
        {
            float barHeight = baseBarHeight + (audioSpectrumValue * multiplier);
            Vector2 size = bar.sizeDelta;
            size.y = Mathf.Clamp(barHeight, 0, maxHeight);
            bar.sizeDelta = Vector2.Lerp(bar.sizeDelta, size, deltaTime * maxSpeed);
        }

        void UpdateBarColor(Image bar, float barHeight)
        {
            bar.color = Color.Lerp(baseColor, loudColor, barHeight * colorSensitivity);
        }
    }
}